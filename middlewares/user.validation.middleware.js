const UserService = require('../services/userService');
const { user } = require('../models/user');

const TYPE_VALID = {
   create: 'create',
   update: 'update',
};

const createUserValid = (req, res, next) => {
   const data = trim(req.body);
   try {
      userValid(data, TYPE_VALID.create);
   } catch (e) {
      res.err = e;
   } finally {
      next();
   }

   // TODO: Implement validatior for user entity during creation
};

const updateUserValid = (req, res, next) => {
   const data = req.body;
   try {
      userValid(data, TYPE_VALID.update);
   } catch (e) {
      res.err = e;
   } finally {
      next();
   }
   // TODO: Implement validatior for user entity during update
};

const userIdValid = (req, res, next) => {
   const isId = UserService.search({ id: req.params.id });
   try {
      if (!isId) {
         throw new Error('User not found');
      }
   } catch (e) {
      res.err = e;
   } finally {
      next();
   }
};

const userValid = (data, type) => {
   if (!userFiledValid(data, type)) {
      throw new ValidationError('Enter excess field(s)');
   }
   if (!Object.values(data).every((value) => value)) {
      throw new ValidationError('Enter all fields');
   }
   if (!validData(data, type)) {
      throw new ValidationError('Enter valid data');
   }
};

const userFiledValid = (data, type) => {
   const { id, ...userFields } = user;
   const userKeys = Object.keys(userFields);
   const dataKeys = Object.keys(data);
   const userKeysEvery = dataKeys.every((key) => userKeys.includes(key));
   if (type === TYPE_VALID.update) {
      return userKeysEvery && dataKeys.length <= userKeys.length;
   }
   return userKeysEvery && dataKeys.length === userKeys.length;
};

const validData = (data, type) => {
   const { password, email, phoneNumber } = data;
   return (
      passwordIsValid(password, type) &&
      emailIsValid(email, type) &&
      phoneIsValid(phoneNumber, type)
   );
};

const passwordIsValid = (password, type) => {
   if (type === TYPE_VALID.update && !password) {
      return true;
   }
   return /^\S{3,}/.test(password);
};

const emailIsValid = (email, type) => {
   if (type === TYPE_VALID.update && !email) {
      return true;
   }
   return /^[^\s@]+@gmail.com$/.test(email);
};

const phoneIsValid = (phone, type) => {
   if (type === TYPE_VALID.update) {
      return true;
   }
   return /^\+380\d{9}$/.test(phone);
};

const trim = (data) => {
   return Object.fromEntries(
      Object.entries(data).map(([k, v]) => {
         if (typeof v === 'string') {
            return [k, v.trim()];
         }
         return [k, v];
      })
   );
};

class ValidationError extends Error {
   constructor(message) {
      super(message);
      this.name = 'ValidationError';
   }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.userIdValid = userIdValid;
exports.trim = trim;
exports.ValidationError = ValidationError;
