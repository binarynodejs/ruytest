const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');
const { ValidationError, trim } = require('./user.validation.middleware');

const TYPE_VALID = {
   create: 'create',
   update: 'update',
};

const createFighterValid = (req, res, next) => {
   try {
      const data = trim(req.body);
      fighterValid(data, TYPE_VALID.create);
   } catch (e) {
      res.err = e;
   } finally {
      next();
   }
   // TODO: Implement validatior for fighter entity during creation
};

const updateFighterValid = (req, res, next) => {
   try {
      const data = trim(req.body);
      fighterValid(data, TYPE_VALID.update);
   } catch (e) {
      res.err = e;
   } finally {
      next();
   }
   // TODO: Implement validatior for fighter entity during update
};

const fighterIdValid = (req, res, next) => {
   const isId = FighterService.search({ id: req.params.id });
   try {
      if (!isId) {
         throw new Error('Fighter not found');
      }
   } catch (e) {
      res.err = e;
   } finally {
      next();
   }
};

const fighterValid = (data, type) => {
   if (!fighterFieldValid(data, type)) {
      throw new ValidationError('Enter excess field(s)');
   }
   if (!Object.values(data).every((value) => value)) {
      throw new ValidationError('Enter all fields');
   }
   if (!validData(data, type)) {
      throw new ValidationError('Enter valid data');
   }
};

const fighterFieldValid = (data, type) => {
   const { id, health, ...fighterFields } = fighter;
   const dataFields = { ...data };
   delete dataFields.health;
   const fighterKeys = Object.keys(fighterFields);
   const dataKeys = Object.keys(dataFields);
   const dataKeysEvery = dataKeys.every((key) => fighterKeys.includes(key));
   if (type === TYPE_VALID.update) {
      return dataKeysEvery && dataKeys.length <= fighterKeys.length;
   }
   return dataKeysEvery && dataKeys.length === fighterKeys.length;
};

const validData = (data, type) => {
   const { health, defense, power } = data;
   return powerIsValid(power, type) && defenseIsValid(defense, type) && healthIsValid(health);
};

const powerIsValid = (power, type) => {
   if (type === TYPE_VALID.update && !power) {
      return true;
   }
   if (isNaN(power)) {
      return false;
   }
   return power >= 1 && power <= 100;
};

const defenseIsValid = (defense, type) => {
   if (type === TYPE_VALID.update && !defense) {
      return true;
   }
   if (isNaN(defense)) {
      return false;
   }
   return defense >= 1 && defense <= 10;
};

const healthIsValid = (health) => {
   if (!health) {
      return true;
   }
   if (isNaN(health)) {
      return false;
   }
   return health >= 80 && health <= 120;
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
exports.fighterIdValid = fighterIdValid;
