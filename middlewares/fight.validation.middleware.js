const { ValidationError } = require('./user.validation.middleware');
const fightService = require('../services/fightService');

const createFightValid = (req, res, next) => {
   try {
      const data = req.body;
      fightValid(data);
   } catch (e) {
      res.err = e;
   } finally {
      next();
   }
};

const fightValid = (data) => {
   if (typeof data !== 'object') {
      throw new ValidationError('wrong field');
   }
   const { fighter1, fighter2, log } = data;
   if (!fighter1 || !fighter2 || !log) {
      throw new ValidationError('wrong field');
   }
};

const fightIdValid = (req, res, next) => {
   try {
      const id = req.params.id;
      const fight = fightService.search({ id });
      if (!fight) {
         throw new Error('fight not find');
      }
   } catch (e) {
      res.err = e;
   } finally {
      next();
   }
};

exports.createFightValid = createFightValid;
exports.fightIdValid = fightIdValid;
