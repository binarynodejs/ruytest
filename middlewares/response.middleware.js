const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
   let status = 200;
   let data = {};
   if (res.err) {
      status = 404;
      const name = res.err.name;
      if (name === 'ValidationError' || name === 'ServiceError') {
         status = 400;
      }
      data = { error: true, message: res.err.message };
   } else if (res.data) {
      data = res.data;
   }
   res.status(status).json(data);
};

exports.responseMiddleware = responseMiddleware;
