import React, { useEffect, useState } from 'react';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { getFight } from '../../services/domainRequest/fightRequest';
import { getFighters } from '../../services/domainRequest/fightersRequest';
import FightsTable from './fightsTable';
import { deleteFight } from '../../services/domainRequest/fightRequest';
import FightTable from './fightTable';
import CustomeButton from './customeButton';

const useStyles = makeStyles({
   root: {
      boxSizing: 'border-box',
      padding: '60px 30px',
   },
});

const Fights = ({ onBack }) => {
   const [loading, setLoading] = useState(true);
   const [fights, setFights] = useState([]);
   const [fighters, setFighters] = useState([]);
   const [fight, setFight] = useState(null);
   const classes = useStyles();
   useEffect(() => {
      let mounted = true;
      const fetchFight = async () => {
         const fights = await getFight();
         const fighters = await getFighters();
         if (!mounted) {
            return;
         }
         setFights(fights);
         setFighters(fighters);
         setLoading(false);
      };
      fetchFight();
      return () => {
         mounted = false;
      };
   }, []);
   const handlerDelete = async (id) => {
      setLoading(true);
      const fightDelete = await deleteFight(id);
      if (typeof fightDelete === 'object' && !fightDelete.error) {
         setFights((state) => {
            return state.filter((fight) => fight.id !== id);
         });
      }
      setLoading(false);
   };
   const handlerClickFight = (fight) => {
      setFight(fight);
   };
   if (loading) {
      return null;
   }
   return (
      <Box className={classes.root}>
         {!fight ? (
            <>
               <FightsTable
                  fights={fights}
                  fighters={fighters}
                  onDelete={handlerDelete}
                  onClickFight={handlerClickFight}
               />
               <CustomeButton onClick={() => onBack()} />
            </>
         ) : (
            <>
               <FightTable fight={fight} fighters={fighters} />
               <CustomeButton onClick={() => setFight(null)} />
            </>
         )}
      </Box>
   );
};

export default Fights;
