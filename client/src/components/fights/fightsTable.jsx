import React from 'react';
import { TableRow, TableCell, Button, TableContainer, Table, TableHead } from '@material-ui/core';
import { TableBody, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const tableField = ['fighter1', 'fighter2'];

const useStyles = makeStyles({
   link: {
      cursor: 'pointer',
   },
});

const FightsTable = ({ fights, fighters, onDelete, onClickFight }) => {
   const classes = useStyles();
   const name = (id) => {
      const fighter = fighters.filter((fighter) => fighter.id === id)[0];
      return (fighter && fighter.name) || 'No name';
   };
   return (
      <TableContainer component={Paper}>
         <Table>
            <TableHead>
               <TableRow>
                  <TableCell>№</TableCell>
                  <TableCell align="right">Fighter 1</TableCell>
                  <TableCell align="right">Fighter 2</TableCell>
                  <TableCell align="right">Date start</TableCell>
                  <TableCell align="right"></TableCell>
               </TableRow>
            </TableHead>
            <TableBody>
               {fights.map((fight, i) => (
                  <TableRow key={i}>
                     <TableCell onClick={() => onClickFight(fight)} className={classes.link}>
                        {i + 1}
                     </TableCell>
                     {tableField.map((field, i) => (
                        <TableCell align="right" key={i}>
                           {name(fight[field])}
                        </TableCell>
                     ))}
                     <TableCell align="right">
                        {new Date(fight.createdAt).toLocaleString()}
                     </TableCell>
                     <TableCell align="center">
                        <Button aria-label="delete" onClick={() => onDelete(fight.id)}>
                           Delete
                        </Button>
                     </TableCell>
                  </TableRow>
               ))}
            </TableBody>
         </Table>
      </TableContainer>
   );
};

export default FightsTable;
