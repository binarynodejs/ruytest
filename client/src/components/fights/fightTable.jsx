import React from 'react';
import { TableRow, TableCell, TableContainer, Table, TableHead } from '@material-ui/core';
import { TableBody, Paper } from '@material-ui/core';

const tableField = ['fighter1Shot', 'fighter2Shot', 'fighter1Health', 'fighter2Health'];

const FightTable = ({ fight, fighters }) => {
   const name = (id) => {
      const fighter = fighters.filter((fighter) => fighter.id === id)[0];
      return (fighter && fighter.name) || 'No name';
   };
   return (
      <TableContainer component={Paper}>
         <Table>
            <TableHead>
               <TableRow>
                  <TableCell>№</TableCell>
                  <TableCell align="right">{`Attack ${name(fight.fighter1)}`}</TableCell>
                  <TableCell align="right">{`Attack ${name(fight.fighter2)}`}</TableCell>
                  <TableCell align="right">{`Health ${name(fight.fighter1)}`}</TableCell>
                  <TableCell align="right">{`Health ${name(fight.fighter2)}`}</TableCell>
               </TableRow>
            </TableHead>
            <TableBody>
               {fight.log.map((step, i) => (
                  <TableRow key={i}>
                     <TableCell>{i + 1}</TableCell>
                     {tableField.map((field, i) => (
                        <TableCell align="right" key={i}>
                           {step[field]}
                        </TableCell>
                     ))}
                  </TableRow>
               ))}
            </TableBody>
         </Table>
      </TableContainer>
   );
};

export default FightTable;
