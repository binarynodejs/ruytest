import React from 'react';
import { Box, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
   root: {
      display: 'flex',
      justifyContent: 'center',
      marginTop: '15px',
   },
});

const CustomeButton = ({ title = 'Back', onClick }) => {
   const classes = useStyles();
   return (
      <Box className={classes.root}>
         <Button variant="contained" color="primary" onClick={() => onClick()}>
            {title}
         </Button>
      </Box>
   );
};

export default CustomeButton;
