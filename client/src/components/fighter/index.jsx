import React, { useState } from 'react';
import { InputLabel, Box, Typography } from '@material-ui/core';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
// import { MenuItem } from 'material-ui';

const useStyles = makeStyles((theme) => ({
   formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
   },
   selectEmpty: {
      marginTop: theme.spacing(2),
   },
}));

export default function Fighter({ fightersList, onFighterSelect, selectedFighter }) {
   const classes = useStyles();
   const [fighter, setFighter] = useState('');

   const handleChange = (event) => {
      //   debugger;
      setFighter(event.target.value);
      onFighterSelect(event.target.value);
   };

   return (
      <Box>
         <FormControl className={classes.formControl}>
            <InputLabel id="simple-select-label">Select Fighter</InputLabel>
            <Select
               labelId="simple-select-label"
               id="simple-select"
               value={fighter}
               onChange={handleChange}
            >
               {fightersList.map((it, index) => {
                  return (
                     <MenuItem key={`${index}`} value={it}>
                        {it.name}
                     </MenuItem>
                  );
               })}
            </Select>
            {selectedFighter ? (
               <Box>
                  <Typography>Name: {selectedFighter.name}</Typography>
                  <Typography>Power: {selectedFighter.power}</Typography>
                  <Typography>Defense: {selectedFighter.defense}</Typography>
                  <Typography>Health: {selectedFighter.health}</Typography>
               </Box>
            ) : null}
         </FormControl>
      </Box>
   );
}
