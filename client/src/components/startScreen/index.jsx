import * as React from 'react';
import { Button, Box } from '@material-ui/core';
import SignInUpPage from '../signInUpPage';
import { isSignedIn } from '../../services/authService';
import SignOut from '../signOut';
import StartFight from './StartFight';
import { Fights } from '../fights';

class StartScreen extends React.Component {
   state = {
      isSignedIn: false,
      allFights: false,
   };

   componentDidMount() {
      this.setIsLoggedIn(isSignedIn());
   }

   hanlderAllFights = (allFights) => {
      this.setState({ allFights });
   };

   setIsLoggedIn = (isSignedIn) => {
      this.setState({ isSignedIn });
   };

   render() {
      const { allFights, isSignedIn } = this.state;
      if (!isSignedIn) {
         return <SignInUpPage setIsLoggedIn={this.setIsLoggedIn} />;
      }

      return (
         <>
            {!allFights ? (
               <>
                  <StartFight />
                  <Box display="flex" justifyContent="center" marginTop="15px">
                     <Button
                        variant="contained"
                        color="primary"
                        onClick={() => this.hanlderAllFights(true)}
                     >
                        Show list fights
                     </Button>
                  </Box>
               </>
            ) : (
               <Fights onBack={this.hanlderAllFights} />
            )}
            <SignOut isSignedIn={isSignedIn} onSignOut={() => this.setIsLoggedIn(false)} />
         </>
      );
   }
}

export default StartScreen;
