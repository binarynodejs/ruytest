import React from 'react';
import Fight from '../fight';
import { Arena } from '../arena';

class StartFight extends React.Component {
   state = {
      fightIsStart: false,
      fighters: [],
   };

   setFightIsStart = (fightIsStart) => {
      this.setState({ fightIsStart });
   };

   setFighters = (fighters) => {
      this.setState({ fighters });
   };

   render() {
      const { fightIsStart, fighters } = this.state;
      return (
         <>
            {fightIsStart ? (
               <Arena fightStart={this.setFightIsStart} fighters={fighters} />
            ) : (
               <Fight fightStart={this.setFightIsStart} fighters={this.setFighters} />
            )}
         </>
      );
   }
}

export default StartFight;
