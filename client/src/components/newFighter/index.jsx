import React, { useState } from 'react';
import { Button, TextField, Box, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { createFighter } from '../../services/domainRequest/fightersRequest';
import './newFighter.css';

const useStyles = makeStyles({
   root: {
      display: 'flex',
      flexDirection: 'column',
   },
});

export default function NewFighter({ onCreated }) {
   const [name, setName] = useState();
   const [power, setPower] = useState();
   const [defense, setDefense] = useState();
   const classes = useStyles();
   const onNameChange = (event) => {
      setName(event.target.value);
   };

   const onPowerChange = (event) => {
      const value =
         event.target.value || event.target.value === 0 ? Number(event.target.value) : null;
      setPower(value);
   };

   const onDefenseChange = (event) => {
      const value =
         event.target.value || event.target.value === 0 ? Number(event.target.value) : null;
      setDefense(value);
   };

   const onSubmit = async () => {
      const data = await createFighter({ name, power, defense });
      if (data && !data.error) {
         onCreated(data);
      }
   };

   return (
      <Box id="new-fighter" className={classes.root}>
         <Typography>New Fighter</Typography>
         <TextField
            onChange={onNameChange}
            id="standard-basic"
            label="Standard"
            placeholder="Name"
         />
         <TextField
            onChange={onPowerChange}
            id="standard-basic"
            label="Standard"
            placeholder="Power"
            type="number"
         />
         <TextField
            onChange={onDefenseChange}
            id="standard-basic"
            label="Standard"
            placeholder="Defense"
            type="number"
         />
         <Button onClick={onSubmit} variant="contained" color="primary">
            Create
         </Button>
      </Box>
   );
}
