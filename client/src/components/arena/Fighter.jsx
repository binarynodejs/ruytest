import React, { useEffect, useState } from 'react';
import { Box, List, ListItem, Paper, Typography, LinearProgress } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';

const fighterField = ['name', 'health', 'power', 'defense'];

const useStyles = makeStyles({
   root: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      width: '250px',
   },
   img: {
      width: '100%',
      height: '250px',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: '20px',
   },
   item: {
      display: 'flex',
      flexWrap: 'nowrap',
      justifyContent: 'space-between',
      width: '100%',
   },
   items: {
      width: '100%',
   },
   progressWrapper: {
      width: '100%',
   },
});

const BorderLinearProgress = withStyles((theme) => ({
   root: {
      height: 10,
      borderRadius: 5,
   },
   colorPrimary: {
      backgroundColor: theme.palette.grey[theme.palette.mode === 'light' ? 200 : 700],
   },
   bar: {
      borderRadius: 5,
      backgroundColor: '#1a90ff',
   },
}))(LinearProgress);

const Fighter = ({ fighterData }) => {
   const classes = useStyles();
   const [health, setHealth] = useState({ current: 100, max: fighterData.health });
   useEffect(() => {
      setHealth((state) => {
         const health = Math.floor((fighterData.health / state.max) * 100);
         return {
            ...state,
            current: health < 0 ? 0 : health,
         };
      });
   }, [fighterData]);
   return (
      <Box className={classes.root}>
         <Box className={classes.progressWrapper}>
            <BorderLinearProgress variant="determinate" value={health.current} />
         </Box>
         <Paper elevation={3} className={classes.img}>
            {fighterData.name}
         </Paper>
         <List className={classes.items}>
            {Object.entries(fighterData)
               .filter(([k, _]) => fighterField.includes(k))
               .map(([k, v]) => (
                  <ListItem key={`${k}-${v}`} className={classes.item}>
                     <Typography>{k}</Typography>
                     <Typography>{v}</Typography>
                  </ListItem>
               ))}
         </List>
      </Box>
   );
};

export default Fighter;
