import { controls } from './controls';

export function getDamage(attacker, defender) {
   const hitPower = getHitPower(attacker);
   const blockPower = getBlockPower(defender);
   const power = hitPower - blockPower;
   return power > 0 ? power : 0;
   // return damage
}

export function getHitPower(fighter) {
   const criticalHitChance = getRandom(1, 2);
   return fighter.power * criticalHitChance;
   // return hit power
}

export function getBlockPower(fighter) {
   const dodgeChance = getRandom(1, 2);
   return fighter.defense * dodgeChance;
   // return block power
}

function getRandom(min, max) {
   return Math.random() * (max - min) + min;
}

export class Fights {
   critAttackCoefficient = 2;
   critTimeot = 10 * 1000;
   constructor(leftFighter, rightFighter, cb) {
      this.leftFighter = {
         ...leftFighter,
         codeBlock: controls.PlayerOneBlock,
         codeAttack: controls.PlayerOneAttack,
         combineKeys: controls.PlayerOneCriticalHitCombination,
         shot: 0,
      };
      this.rightFighter = {
         ...rightFighter,
         codeBlock: controls.PlayerTwoBlock,
         codeAttack: controls.PlayerTwoAttack,
         combineKeys: controls.PlayerTwoCriticalHitCombination,
         shot: 0,
      };
      this.controls = controls;
      this.checkHitsBind = this.checkHits.bind(this);
      this.listener = new Listener(controls, this.checkHitsBind);
      this.cb = cb;
      this.log = new Log();
   }
   checkHits(codes) {
      [
         ['left', 'right'],
         ['right', 'left'],
      ].forEach(([att, def]) => {
         const attacker = this[`${att}Fighter`];
         const defender = this[`${def}Fighter`];
         attacker.shot = 0;
         this.hit(attacker, defender, codes);
         this.critHit(attacker, defender, codes);
      });
      if (this.leftFighter.health <= 0 || this.rightFighter.health <= 0) {
         this.listener.removeListener();
      }
      this.cb(this.leftFighter, this.rightFighter, this.log.log);
   }
   hit(att, def, codes) {
      if (!codes.has(att.codeAttack) || codes.has(att.codeBlock) || codes.has(def.codeBlock)) {
         return;
      }
      att.shot = getDamage(att, def);
      def.health -= att.shot;
      this.createLog();
   }
   critHit(att, def, codes) {
      if (!att.combineKeys.every((code) => codes.has(code)) || att.crit) {
         return;
      }
      att.shot = this.critAttackCoefficient * att.power;
      def.health -= att.shot;
      att.crit = true;
      const timeout = setTimeout(() => {
         clearTimeout(timeout);
         att.crit = false;
      }, this.critTimeot);
      this.createLog();
   }
   createLog() {
      this.log.hit(this.leftFighter, this.rightFighter);
   }
}

class Log {
   constructor() {
      this.log = [];
   }
   hit(one, two) {
      this.log.push({
         fighter1Shot: one.shot,
         fighter2Shot: two.shot,
         fighter1Health: one.health <= 0 ? 0 : one.health,
         fighter2Health: two.health <= 0 ? 0 : two.health,
      });
   }
}

class Listener {
   constructor(keysCode, cb) {
      this.allKeysCode = [
         ...Object.values(keysCode).reduce(
            (newKeys, key) => [...newKeys, ...(Array.isArray(key) ? key : [key])],
            []
         ),
      ];
      this.keysPress = new Set();
      this.body = document.querySelector('body');
      this.handlerKeyDownBind = this.handlerKeyDown.bind(this);
      this.handlerKeyUpBind = this.handlerKeyUp.bind(this);
      this.cb = cb;
      this.addListener();
   }
   addListener() {
      this.body.addEventListener('keydown', this.handlerKeyDownBind);
      this.body.addEventListener('keyup', this.handlerKeyUpBind);
   }
   removeListener() {
      this.body.removeEventListener('keydown', this.handlerKeyDownBind);
      this.body.removeEventListener('keyup', this.handlerKeyUpBind);
   }
   handlerKeyDown(event) {
      const code = event.code;
      if (!this.allKeysCode.includes(code) || this.keysPress.has(code)) {
         return;
      }
      event.preventDefault();
      this.keysPress.add(code);
      this.cb(this.keysPress);
   }
   handlerKeyUp(event) {
      this.keysPress.delete(event.code);
   }
}
