import React, { useEffect, useState } from 'react';
import { Box, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Fight from './Fight.jsx';
import { createFight } from '../../services/domainRequest/fightRequest';
import WinnerModal from './Winner';

const useStyles = makeStyles({
   root: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      flexWrap: 'nowrap',
      margin: '60px 50px 0',
   },
   buttonWrapper: {
      marginTop: '20px',
      display: 'flex',
      justifyContent: 'center',
   },
});

const Arena = ({ fighters, fightStart }) => {
   const classes = useStyles();
   const [winner, setWinner] = useState(null);
   const handlerClickBack = () => {
      fightStart(false);
   };
   const handlerWinner = (winner, log) => {
      createFight({
         fighter1: fighters[0].id,
         fighter2: fighters[1].id,
         log,
      });
      setWinner(winner);
   };
   return (
      <>
         <Fight className={classes.root} fightersData={fighters} handlerWinner={handlerWinner} />
         <Box className={classes.buttonWrapper}>
            <Button variant="outlined" size="large" onClick={handlerClickBack}>
               Back
            </Button>
         </Box>
         {winner && <WinnerModal close={() => setWinner(null)} winner={winner} />}
      </>
   );
};

export default Arena;
