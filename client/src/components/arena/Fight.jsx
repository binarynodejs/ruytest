import React, { useEffect, useState } from 'react';
import { Box, Typography } from '@material-ui/core';
import Fighter from './Fighter';
import { Fights } from './fight';

const Fight = ({ fightersData, handlerWinner, className }) => {
   const [fighters, setFighters] = useState(fightersData);
   const handlerKick = async (left, right, log) => {
      if (left.health <= 0 || right.health <= 0) {
         let winner = left.health <= 0 ? right : left;
         if (left.health <= 0 && right.health <= 0) {
            winner = 'draw';
         }
         handlerWinner(winner, log);
      }
      setFighters((state) => {
         return [
            {
               ...state[0],
               health: left.health < 0 ? 0 : Math.floor(left.health),
            },
            {
               ...state[1],
               health: right.health < 0 ? 0 : Math.floor(right.health),
            },
         ];
      });
   };
   useEffect(() => {
      let fights = new Fights(...fightersData, handlerKick);
      return () => {
         fights = null;
      };
   }, [fightersData]);
   return (
      <Box className={className}>
         <Fighter fighterData={fighters[0]} />
         <Typography variant="h1">VS</Typography>
         <Fighter fighterData={fighters[1]} />
      </Box>
   );
};

export default Fight;
