import React, { useState } from 'react';
import { Box, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CustomeModal from '../modal';
import Fighter from './Fighter';

const useStyles = makeStyles({
   wrapperFighter: {
      boxSizing: 'border-box',
      padding: '20px',
   },
});

const WinnerModal = ({ winner, close }) => {
   const classes = useStyles();
   const content =
      typeof winner !== 'string' ? (
         <Box className={classes.wrapperFighter}>
            <Typography variant="h4">Winner</Typography>
            <Fighter fighterData={winner} />
         </Box>
      ) : (
         <Typography variant="h3" className={classes.wrapperFighter}>
            {winner}
         </Typography>
      );
   const [open, setOpen] = useState(true);
   const handlerClose = () => {
      setOpen(false);
      close();
   };
   return (
      <CustomeModal open={open} close={handlerClose}>
         {content}
      </CustomeModal>
   );
};

export default WinnerModal;
