import React, { useState } from 'react';
import { Box, Modal } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
   root: {
      position: 'absolute',
      left: '50%',
      top: '50%',
      transform: 'translate(-50%, -50%)',	
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
   },
}));

const CustomeModal = ({ children, open, close }) => {
   const classes = useStyles();
   const handlerOpen = () => {
      // setOpen(false);
      close();
   };
   return (
      <Modal open={open} onClose={handlerOpen}>
         <Box className={classes.root}>{children}</Box>
      </Modal>
   );
};

export default CustomeModal;
