const { UserRepository } = require('../repositories/userRepository');
const { BaseService, ServiceError } = require('./baseService');

class UserService extends BaseService {
   // TODO: Implement methods to work with user
   all() {
      return UserRepository.getAll();
   }
   create(data) {
      const user = this.trim(data);
      if (this.emailIsExist(user.email) || this.phoneIsExist(user.phoneNumber)) {
         throw new ServiceError('User already exists');
      }
      return UserRepository.create(user);
   }
   search(search) {
      const item = UserRepository.getOne(search);
      if (!item) {
         return null;
      }
      return item;
   }
   delete(id) {
      return UserRepository.delete(id);
   }
   update(id, dataOld) {
      const user = this.search({ id });
      const data = this.trim(dataOld);
      if (
         (user.email !== data.email && this.emailIsExist(data.email)) ||
         (user.phoneNumber !== data.phoneNumber && this.phoneIsExist(data.phoneNumber))
      ) {
         throw new ServiceError(`User already exists`);
      }
      return UserRepository.update(id, data);
   }
   emailIsExist(email) {
      return this.search({ email });
   }
   phoneIsExist(phoneNumber) {
      return this.search({ phoneNumber });
   }
}

module.exports = new UserService();
