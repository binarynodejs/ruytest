class BaseService {
   trim(data) {
      return Object.fromEntries(
         Object.entries(data).map(([k, v]) => {
            if (typeof v === 'string') {
               return [k, v.trim()];
            }
            return [k, v];
         })
      );
   }
}

class ServiceError extends Error {
   constructor(message) {
      super(message);
      this.name = 'ServiceError';
   }
}

exports.BaseService = BaseService;
exports.ServiceError = ServiceError;
