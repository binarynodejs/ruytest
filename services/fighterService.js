const { FighterRepository } = require('../repositories/fighterRepository');
const { BaseService, ServiceError } = require('./baseService');

const HEALTH_DEFAULT = 100;

class FighterService extends BaseService {
   create(data) {
      const fighter = this.trim(data);
      if (this.nameIsExist(fighter.name)) {
         throw new ServiceError('Fighter already exists');
      }
      return FighterRepository.create({ health: HEALTH_DEFAULT, ...fighter });
   }
   all() {
      return FighterRepository.getAll();
   }
   update(id, dataOld) {
      const fighter = this.search({ id });
      const data = this.trim(dataOld);
      if (fighter.name !== data.name && this.nameIsExist(data.name)) {
         throw new ServiceError(`Fighter already exists`);
      }
      return FighterRepository.update(id, data);
   }
   delete(id) {
      return FighterRepository.delete(id);
   }
   search(search) {
      const item = FighterRepository.getOne(search);
      if (!item) {
         return null;
      }
      return item;
   }
   nameIsExist(name) {
      return this.search({ name });
   }
   // TODO: Implement methods to work with fighters
}

module.exports = new FighterService();
