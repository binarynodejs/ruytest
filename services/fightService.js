const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
   create(data) {
      return FightRepository.create(data);
   }
   all() {
      return FightRepository.getAll();
   }
   search(search) {
      const fight = FightRepository.getOne(search);
      return !fight ? null : fight;
   }
	delete(id) {
      return FightRepository.delete(id);
   }
   // OPTIONAL TODO: Implement methods to work with fights
}

module.exports = new FightersService();
