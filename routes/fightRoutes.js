const { Router } = require('express');
const FightService = require('../services/fightService');
const { createFightValid, fightIdValid } = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post(
   '/',
   createFightValid,
   (req, res, next) => {
      try {
         if (!res.err) {
            const { fighter1, fighter2, log } = req.body;
            res.data = FightService.create({ fighter1, fighter2, log });
         }
      } catch (error) {
         res.err = error;
      } finally {
         next();
      }
   },
   responseMiddleware
);

router.get(
   '/',
   (_, res, next) => {
      res.data = FightService.all();
      next();
   },
   responseMiddleware
);

router.get(
   '/:id',
   fightIdValid,
   (req, res, next) => {
      if (!res.err) {
         res.data = FightService.search({ id: req.params.id });
      }
      next();
   },
   responseMiddleware
);

router.delete(
   '/:id',
   fightIdValid,
   (req, res, next) => {
      if (!res.err) {
         res.data = FightService.delete(req.params.id);
      }
      next();
   },
   responseMiddleware
);

// OPTIONAL TODO: Implement route controller for fights

module.exports = router;
