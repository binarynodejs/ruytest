const { Router } = require('express');
const UserService = require('../services/userService');
const {
   createUserValid,
   updateUserValid,
   userIdValid,
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post(
   '/',
   createUserValid,
   (req, res, next) => {
      try {
         if (!res.err) {
            res.data = UserService.create(req.body);
         }
      } catch (error) {
         res.err = error;
      } finally {
         next();
      }
   },
   responseMiddleware
);

router.get(
   '/:id',
   userIdValid,
   (req, res, next) => {
      if (!res.err) {
         res.data = UserService.search({ id: req.params.id });
      }
      next();
   },
   responseMiddleware
);

router.get(
   '/',
   (_, res, next) => {
      res.data = UserService.all();
      next();
   },
   responseMiddleware
);

router.put(
   '/:id',
   userIdValid,
   updateUserValid,
   (req, res, next) => {
      try {
         if (!res.err) {
            res.data = UserService.update(req.params.id, req.body);
         }
      } catch (e) {
         res.err = e;
      } finally {
         next();
      }
   },
   responseMiddleware
);

router.delete(
   '/:id',
   userIdValid,
   (req, res, next) => {
      if (!res.err) {
         res.data = UserService.delete(req.params.id);
      }
      next();
   },
   responseMiddleware
);

// TODO: Implement route controllers for user

module.exports = router;
