const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {
   createFighterValid,
   updateFighterValid,
   fighterIdValid,
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.post(
   '/',
   createFighterValid,
   (req, res, next) => {
      try {
         if (!res.err) {
            res.data = FighterService.create(req.body);
         }
      } catch (error) {
         res.err = error;
      } finally {
         next();
      }
   },
   responseMiddleware
);

router.get(
   '/',
   (_, res, next) => {
      res.data = FighterService.all();
      next();
   },
   responseMiddleware
);

router.get(
   '/:id',
   fighterIdValid,
   (req, res, next) => {
      if (!res.err) {
         res.data = FighterService.search({ id: req.params.id });
      }
      next();
   },
   responseMiddleware
);

router.put(
   '/:id',
   fighterIdValid,
   updateFighterValid,
   (req, res, next) => {
      try {
         if (!res.err) {
            res.data = FighterService.update(req.params.id, req.body);
         }
      } catch (e) {
         res.err = e;
      } finally {
         next();
      }
   },
   responseMiddleware
);

router.delete(
   '/:id',
   fighterIdValid,
   (req, res, next) => {
      if (!res.err) {
         res.data = FighterService.delete(req.params.id);
      }
      next();
   },
   responseMiddleware
);

// TODO: Implement route controllers for fighter

module.exports = router;
